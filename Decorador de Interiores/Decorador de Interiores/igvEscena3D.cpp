#include <cstdlib>
#include <stdio.h>

#include "igvEscena3D.h"
#include "igvFuenteLuz.h"
#include "igvMaterial.h"
#include "igvTextura.h"
#include "Objeto.hpp"
#include "CamaraVigilancia.hpp"
#include "Silla.hpp"
#include "Mesa.hpp"

// Metodos constructores 

igvEscena3D::igvEscena3D () {
	ejes = true;
    
    int r,g,b;
    
    //Color cálido equivalente a 3500K
    r = 255; g = 196; b = 137;
    
    difusoR = igvColor((float) r/255, (float) g/255, (float) b/255);
    especularR = igvColor((float) r/255, (float) g/255, (float) b/255);
    ambienteR = igvColor((float) r/255, (float) g/255, (float) b/255);
    phongR = 120;
    
    tam_x = 20; tam_y = 3.0; tam_z = 20; //Dimensiones de la habitación
    
    //Posición del personaje
    PersonajeZ = 15.0;
    PersonajeX = 15.0;
    anguloPersonaje = 90;
    
    //Cámara de vigilancia siempre creada por defecto
    objetos.push_back(new class CamaraVigilancia(CAMARA,0,0.1,0.3));
    
    //Algunos objetos por defecto
    objetos.push_back(new class Silla(SUELO,0.5,0.1,0.5, PersonajeX, 0.1, PersonajeZ, anguloPersonaje,0.2, 0.3, 0.4,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/sofa.jpg"));
    objetos.push_back(new class Silla(SUELO,0.5,0.1,0.5, PersonajeX+4, 0.1, PersonajeZ, anguloPersonaje,1, 0.2, 0.9,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/piel.jpg"));
    objetos.push_back(new class Mesa(SUELO,1,0.1,1, PersonajeX+4, 0.1, PersonajeZ+2, anguloPersonaje,1, 1, 0.4,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg"));
    
}

igvEscena3D::~igvEscena3D() {
}

// Metodos publicos 

void pintar_ejes(void) {
  GLfloat rojo[]={1,0,0,1.0};
  GLfloat verde[]={0,1,0,1.0};
  GLfloat azul[]={0,0,1,1.0};

  glMaterialfv(GL_FRONT,GL_EMISSION,rojo);
	glBegin(GL_LINES);
		glVertex3f(1000,0,0);
		glVertex3f(-1000,0,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,verde);
	glBegin(GL_LINES);
		glVertex3f(0,1000,0);
		glVertex3f(0,-1000,0);
	glEnd();

  glMaterialfv(GL_FRONT,GL_EMISSION,azul);
	glBegin(GL_LINES);
		glVertex3f(0,0,1000);
		glVertex3f(0,0,-1000);
	glEnd();
}

void igvEscena3D::base(float div_x, float div_z, int y){
    float incrementoX = tam_x / div_x, incrementoZ = tam_z / div_z; //Incremento para las posiciones del objeto
    float incrementoTexturaX = 1 / div_x, incrementoTexturaZ = 1 / div_z; //Incremento para la textura
    float posX = 0, posY = 0; // coordenadas de textura
    
    glPushMatrix();
    for(float i = 0; i< tam_x; i+=incrementoX, posX += incrementoTexturaX){
        posY = 0;
        for(float j = 0; j<tam_z; j+=incrementoZ, posY += incrementoTexturaZ){
            glNormal3f(0, 1, 0);
            glBegin(GL_QUADS);
                glTexCoord2f(posX, posY);
                glVertex3f(i, y, j);

                glTexCoord2f(posX,posY + incrementoTexturaZ);
                glVertex3f(i, y, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY + incrementoTexturaZ);
                glVertex3f(i+incrementoX, y, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY);
                glVertex3f(i+incrementoX, y, j);
            glEnd();
        }
    }
    glPopMatrix();
}

void igvEscena3D::paredes(float div_x, float div_z){
    float incrementoX = tam_x / div_x, incrementoZ = tam_x / div_z; //Incremento para las posiciones del objeto
    float incrementoTexturaX = 1 / div_x, incrementoTexturaZ = 1 / div_z; //Incremento para la textura
    float posX = 0, posY = 0; // coordenadas de textura
    
    igvTextura textura1("/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/pared.jpg");
    textura1.aplicar();
    
    igvMaterial material(igvColor(1,0.6,0.6), igvColor(1,0.5,0.3), igvColor(1,0.4,1), phongR); //Componentes Parametrizados
    material.aplicar();
    //Pared 1
    for(float i = 0; i<tam_x; i+=incrementoX, posX += incrementoTexturaX){
        posY = 0;
        for(float j = 0; j<tam_y; j+=incrementoZ, posY += incrementoTexturaZ){
            glNormal3f(0,1,0);
            glBegin(GL_QUADS);
                glTexCoord2f(posX,posY);
                glVertex3f(i, j, 0.0);

                glTexCoord2f(posX,posY + incrementoTexturaZ);
                glVertex3f(i, j+incrementoZ, 0.0);

                glTexCoord2f(posX + incrementoTexturaX, posY + incrementoTexturaZ);
                glVertex3f(i+incrementoX, j+incrementoZ, 0.0);

                glTexCoord2f(posX + incrementoTexturaX, posY);
                glVertex3f(i+incrementoX, j, 0.0);
            glEnd();
        }
    }

    posX = 0, posY = 0;
    //Pared 2
    for(float i = 0; i<tam_y; i+=incrementoX, posX += incrementoTexturaX){
        posY = 0;
        for(float j = 0; j<tam_z; j+=incrementoZ, posY += incrementoTexturaZ){
            glNormal3f(0,1,0);
            glBegin(GL_QUADS);
                glTexCoord2f(posX,posY);
                glVertex3f(0.0, i, j);

                glTexCoord2f(posX,posY + incrementoTexturaZ);
                glVertex3f(0.0, i, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY + incrementoTexturaZ);
                glVertex3f(0.0, i+incrementoX, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY);
                glVertex3f(0.0, i+incrementoX, j);
            glEnd();
        }
    }
    posX = 0; posY = 0;
    
    //Pared 3
    for(float i = 0; i<tam_x; i+=incrementoX, posX += incrementoTexturaX){
        posY = 0;
        for(float j = 0; j<tam_y; j+=incrementoZ, posY += incrementoTexturaZ){
            glNormal3f(0,1,0);
            glBegin(GL_QUADS);
                glTexCoord2f(posX,posY);
                glVertex3f(i, j, tam_z);

                glTexCoord2f(posX,posY + incrementoTexturaZ);
                glVertex3f(i, j+incrementoZ, tam_z);

                glTexCoord2f(posX + incrementoTexturaX, posY + incrementoTexturaZ);
                glVertex3f(i+incrementoX, j+incrementoZ, tam_z);

                glTexCoord2f(posX + incrementoTexturaX, posY);
                glVertex3f(i+incrementoX, j, tam_z);
            glEnd();
        }
    }

    posX = 0; posY = 0;
    //Pared 4
    for(float i = 0; i<tam_y; i+=incrementoX, posX += incrementoTexturaX){
        posY = 0;
        for(float j = 0; j<tam_z; j+=incrementoZ, posY += incrementoTexturaZ){
            glNormal3f(0,1,0);
            glBegin(GL_QUADS);
                glTexCoord2f(posX,posY);
                glVertex3f(tam_x, i, j);

                glTexCoord2f(posX,posY + incrementoTexturaZ);
                glVertex3f(tam_x, i, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY + incrementoTexturaZ);
                glVertex3f(tam_x, i+incrementoX, j+incrementoZ);

                glTexCoord2f(posX + incrementoTexturaX, posY);
                glVertex3f(tam_x, i+incrementoX, j);
            glEnd();
        }
    }
}

void igvEscena3D::visualizar(void) {
	// crear el modelo
	glPushMatrix(); // guarda la matriz de modelado

    // se pintan los ejes
    if (ejes) pintar_ejes();

    igvFuenteLuz luzPuntual(GL_LIGHT0, igvPunto3D(tam_x/2,5,tam_z/2), ambienteR, difusoR, especularR, 1,0,0);
    luzPuntual.aplicar();
    
    glPushMatrix();
        igvTextura textura("/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/suelo.jpg");
        textura.aplicar();

        igvMaterial material(igvColor(0.5,0.25,0.15), igvColor(0.5,0.25,0.15), igvColor(0.5,0.25,0.15), phongR); //Componentes Parametrizados
        material.aplicar();
        base(500,500,0.0);
    
        igvTextura textura1("/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/techo.jpg");
        textura1.aplicar();
    
        igvMaterial material1(igvColor(0.7,0.8,1), igvColor(0.7,0.8,1), igvColor(0.7,0.8,1), phongR); //Componentes Parametrizados
        material1.aplicar();
        base(500, 500, tam_y);
        paredes(500, 500);
    glPopMatrix();

    for(int i=0; i<objetos.size();i++){
        igvMaterial material2(igvColor(1,0.8,0.6), igvColor(0.5,0.5,0.5), igvColor(0.5,0.5,0.5), 60); //Componentes Parametrizados
        material2.aplicar();
        
        igvTextura textura2(objetos[i]->getRuta());
        textura2.aplicar(); //Cargamos las texturas

        objetos[i]->visualizar();
    }
    

	glPopMatrix (); // restaura la matriz de modelado
}

void igvEscena3D::setDifusoR(float incremento){
    difusoR[0] += incremento;
    difusoR[1] += incremento;
    difusoR[2] += incremento;
}

void igvEscena3D::setEspecularR(float incremento){
    especularR[0] += incremento;
    especularR[1] += incremento;
    especularR[2] += incremento;
}

void igvEscena3D::rotPersonaje(float valor){
    anguloPersonaje += valor;
}

void igvEscena3D::avanzar(float valor){
    igvPunto3D referencia;
    float d1 = -sin(anguloPersonaje);
    float d2 = cos(anguloPersonaje);

    // Actualiza la posición
    PersonajeX += valor*d1;
    PersonajeZ +=  valor*d2;
}

bool igvEscena3D::colision(float x, float z, float posicionX, float posicionZ){

    for(int i=0;i<objetos.size();i++){
        if(objetos[i]->getTipoObjeto() == SUELO){
            
            if( x == -1 && z == -1){ //Si el objeto se va a colocar nuevo
                x = objetos[i]->tamX()/2;
                z = objetos[i]->tamZ()/2;
            }
            
            float limiteInferiorX = objetos[i]->getPosX() - objetos[i]->tamX()/2;
            float limiteInferiorZ = objetos[i]->getPosZ() - objetos[i]->tamZ()/2;
            float limiteSuperiorX = objetos[i]->getPosX() + objetos[i]->tamX()/2;
            float limiteSuperiorZ = objetos[i]->getPosZ() + objetos[i]->tamZ()/2;
            
            if((x + posicionX >= limiteInferiorX &&  z + posicionZ <= limiteSuperiorZ) && (x + posicionX <=  limiteSuperiorX && z + posicionZ >= limiteInferiorZ )){
                
                return false;
            }
        }
    }
    
    return true;
}
