#ifndef __IGVESCENA3D
#define __IGVESCENA3D

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

#include "igvFuenteLuz.h"
#include "igvMaterial.h"
#include "igvTextura.h"
#include "Objeto.hpp"
#include <vector>

class igvEscena3D {
	protected:
		// Atributos
		bool ejes;
        igvColor difusoR, especularR, ambienteR;
        double phongR;
        float tam_x, tam_y, tam_z;
        
        igvPunto3D dirFoco;
    
        float PersonajeZ;
        float PersonajeX;
        float anguloPersonaje;
    
        std::vector<Objeto*> objetos; //Array con todas los objetos de la escena
    
	public:

		// Constructores por defecto y destructor
		igvEscena3D();
		~igvEscena3D();

		// Metodos estáticos

		// Métodos
		// método con las llamadas OpenGL para visualizar la escena
        void visualizar();

        bool get_ejes() {return ejes;};
        void set_ejes(bool _ejes){ejes = _ejes;};
        void setDifusoR(float incremento);
        void setEspecularR(float incremento);
        void setPhongR(double incremento){ phongR += incremento;}
        
        void setAlturaFoco(float incremento){ dirFoco[1] += incremento; }
        void setHorizontalFoco(float incremento){ dirFoco[0] += incremento; }
    
        //Métodos para dibujar la estructura de la habitación mediante malla de cuadrados
        void base(float div_x, float div_z, int y); //Dibuja el techo o suelo, indicando el eje y
        void paredes(float div_x, float div_y); //Dibuja una pared con unas coordenadas de inicio y fin
    
        void avanzar(float distancia);
        void rotPersonaje(float valor);
        double get_PersonajeZ() {return PersonajeZ;};
        double get_PersonajeX() {return PersonajeX;};
        double get_anguloPersonaje() {return anguloPersonaje;};
        float getLimiteX(){ return tam_x; }
        float getLimiteZ(){ return tam_z; }
    
        std::vector<Objeto*>& getObjetos() { return objetos; };
        bool colision(float x, float z, float posicionX, float posicionZ); //Devuelve true si el objeto se puede colocar en una posición x,z
    
        float getTam_x(){ return tam_x; }
        float getTam_y(){ return tam_y; }
        float getTam_z(){ return tam_z; }
};

#endif
