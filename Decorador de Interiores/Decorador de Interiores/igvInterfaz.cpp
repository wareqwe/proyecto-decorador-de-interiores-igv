#include <cstdlib>
#include <stdio.h>

#include "igvInterfaz.h"
#include "Objetos/Objeto.hpp"
#include "Objetos/Mesa.hpp"
#include "Objetos/Silla.hpp"
#include "Objetos/Cuadro.hpp"
#include "Objetos/Alfombra.hpp"

#include <iostream>

#define GL_SILENCE_DEPRECATION

extern igvInterfaz interfaz; // los callbacks deben ser estaticos y se requiere este objeto para acceder desde
                             // ellos a las variables de la clase

// Metodos constructores -----------------------------------

igvInterfaz::igvInterfaz () {
    modo = IGV_VISUALIZAR;
    boton_retenido = false;
    interfaz.objeto_seleccionado = -1;
}

igvInterfaz::~igvInterfaz () {}


// Metodos publicos ----------------------------------------

void igvInterfaz::crear_mundo(void) {
	// crear c�maras
    interfaz.camara.set(IGV_PERSPECTIVA, igvPunto3D(19, 2.5, 19), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0), -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);

    //par�metros de la perspectiva
    interfaz.camara.angulo = 60.0;
    interfaz.camara.raspecto = 1.0;
}

void igvInterfaz::configura_entorno(int argc, char** argv,
			                              int _ancho_ventana, int _alto_ventana,
			                              int _pos_X, int _pos_Y,
													          string _titulo){
	// inicializaci�n de las variables de la interfaz																	
	ancho_ventana = _ancho_ventana;
	alto_ventana = _alto_ventana;

	// inicializaci�n de la ventana de visualizaci�n
	glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(_ancho_ventana,_alto_ventana);
  glutInitWindowPosition(_pos_X,_pos_Y);
	glutCreateWindow(_titulo.c_str());

	glEnable(GL_DEPTH_TEST); // activa el ocultamiento de superficies por z-buffer
  glClearColor(1.0,1.0,1.0,0.0); // establece el color de fondo de la ventana

	glEnable(GL_LIGHTING); // activa la iluminacion de la escena
  glEnable(GL_NORMALIZE); // normaliza los vectores normales para calculo iluminacion

	glEnable(GL_TEXTURE_2D); // activa el uso de texturas

    create_menu();
	crear_mundo(); // crea el mundo a visualizar en la ventana
}

void igvInterfaz::inicia_bucle_visualizacion() {
	glutMainLoop(); // inicia el bucle de visualizacion de OpenGL
}

void igvInterfaz::set_glutSpecialFunc(int key, int x, int y) {
	// Apartado F: manejo de las teclas especiales del teclado para mover la posici�n del foco
    if(interfaz.objeto_seleccionado != -1){
        
        float posicionX = interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosX();
        float posicionZ = interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosZ();
        
        float bordeObjetoX = interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamX() + 1;
        float bordeObjetoZ = interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamZ() + 1;
        
        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == SUELO){
            
            switch (key) { //Objetos de tipo SUELO
                case GLUT_KEY_UP:
                    
                    //Comprobamos que no se exceden los l�mites de la habitaci�n
                    if(posicionX + round(-sin(interfaz.escena.get_anguloPersonaje())) + bordeObjetoX < interfaz.escena.getLimiteX() && posicionZ + round(cos(interfaz.escena.get_anguloPersonaje())) + bordeObjetoZ < interfaz.escena.getLimiteZ() &&
                       
                       posicionX + round(-sin(interfaz.escena.get_anguloPersonaje())) + bordeObjetoX > 2 && posicionZ + round(cos(interfaz.escena.get_anguloPersonaje())) + bordeObjetoZ > 2){
                    
                        if(interfaz.escena.colision(round(-sin(interfaz.escena.get_anguloPersonaje())), round(cos(interfaz.escena.get_anguloPersonaje())), posicionX, posicionZ)){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->mover(round(-sin(interfaz.escena.get_anguloPersonaje())), round(cos(interfaz.escena.get_anguloPersonaje())));
                        }
                    }
                    break;
                case GLUT_KEY_DOWN:
                    
                    //Comprobamos que no se exceden los l�mites de la habitaci�n
                    if(posicionX + round(sin(interfaz.escena.get_anguloPersonaje())) + bordeObjetoX< interfaz.escena.getLimiteX() && posicionZ + round(-cos(interfaz.escena.get_anguloPersonaje())) + bordeObjetoZ < interfaz.escena.getLimiteZ() &&
                       
                       posicionX + round(sin(interfaz.escena.get_anguloPersonaje())) + bordeObjetoX > 2 && posicionZ + round(-cos(interfaz.escena.get_anguloPersonaje())) + bordeObjetoZ > 2){
                        
                        if(interfaz.escena.colision(round(sin(interfaz.escena.get_anguloPersonaje())), round(-cos(interfaz.escena.get_anguloPersonaje())), posicionX, posicionZ)){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->mover(round(sin(interfaz.escena.get_anguloPersonaje())), round(-cos(interfaz.escena.get_anguloPersonaje())));
                        }
                    }
                    break;
                case GLUT_KEY_LEFT:
                    
                    //Comprobamos que no se exceden los l�mites de la habitaci�n
                    if(posicionX + round(sin(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoX< interfaz.escena.getLimiteX() && posicionZ + round(-cos(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoZ < interfaz.escena.getLimiteZ() &&
                       
                       posicionX + round(sin(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoX > 2 && posicionZ + round(-cos(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoZ > 2){
                        
                        if(interfaz.escena.colision(round(sin(interfaz.escena.get_anguloPersonaje()+1.5708)), round(-cos(interfaz.escena.get_anguloPersonaje()+1.5708)), posicionX, posicionZ)){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->mover(round(sin(interfaz.escena.get_anguloPersonaje()+1.5708)), round(-cos(interfaz.escena.get_anguloPersonaje()+1.5708)));
                        }
                    }
                    break;
                case GLUT_KEY_RIGHT:
                    //Comprobamos que no se exceden los l�mites de la habitaci�n
                    if(posicionX + round(-sin(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoX< interfaz.escena.getLimiteX() && posicionZ + round(cos(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoZ < interfaz.escena.getLimiteZ() &&
                       
                       posicionX + round(-sin(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoX > 2 && posicionZ + round(cos(interfaz.escena.get_anguloPersonaje()+1.5708)) + bordeObjetoZ > 2){
                        
                        if(interfaz.escena.colision(round(-sin(interfaz.escena.get_anguloPersonaje()+1.5708)), round(cos(interfaz.escena.get_anguloPersonaje()+1.5708)), posicionX, posicionZ)){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->mover(round(-sin(interfaz.escena.get_anguloPersonaje()+1.5708)), round(cos(interfaz.escena.get_anguloPersonaje()+1.5708)));
                        }
                    }
                    break;
            }
        }else{
            switch (key) { //Objetos de tipo PARED
                case GLUT_KEY_UP:
                    if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosY() + (interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamY()/2) <= interfaz.escena.getTam_y()){
                        
                        interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(0, 0.2, 0);
                    }
                    break;
                case GLUT_KEY_DOWN:
                    if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosY() - interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamY() - 0.2 >= 0){
                        
                        interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(0, -0.2, 0);
                    }
                    break;
                case GLUT_KEY_LEFT:
                    if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == PARED1 || interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == PARED2){
                        
                        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosX() - interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamX()/2 - 0.2 >= 0){
                        
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(-0.2, 0, -0.2);
                        }
                    }else{
                        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosX() + interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamX()/2 + 0.2 <= interfaz.escena.getTam_x()){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(0.2, 0, 0.2);
                        }
                    }
                    break;
                case GLUT_KEY_RIGHT:
                    if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == PARED1 || interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == PARED2){
                        
                        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosX() + interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamX()/2 + 0.2 <= interfaz.escena.getTam_x()){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(0.2, 0, 0.2);
                        }
                    }else{
                        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getPosX() - interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->tamX()/2 - 0.2 >= 0){
                            
                            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->moverPared(-0.2, 0, -0.2);
                        }
                    }
                    break;
            }
        }
    }
    glutPostRedisplay(); // renueva el contenido de la ventana de vision
}

void igvInterfaz::set_glutKeyboardFunc(unsigned char key, int x, int y) {
    //Generamos cajas con colores aleatorios
    int xColor = rand()%256, yColor = rand()%256, zColor = rand()%256;
    float conversionX = (float) xColor/255, conversionY = (float) yColor/255, conversionZ = (float) zColor/255;
	switch (key) {
		case 'p': // Apartado E: aumentar en 10 el exponente de Phong del material
            if(interfaz.camara.tipo==IGV_PERSPECTIVA){
                interfaz.camara.set(IGV_PERSONA, igvPunto3D(interfaz.escena.get_PersonajeX(),1.0,interfaz.escena.get_PersonajeZ()),igvPunto3D(-sin(interfaz.escena.get_anguloPersonaje())*200,0,cos(interfaz.escena.get_anguloPersonaje())*200),igvPunto3D(0,1,0), -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
            }else{
                interfaz.camara.set(IGV_PERSPECTIVA, igvPunto3D(19, 2.5, 19), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0), -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
            }
            interfaz.camara.aplicar();
		break;
        case '+': // zoom in
                interfaz.camara.zoom(-0.05); //Hacemos m�s peque�a la ventana
        break;
        case '-': // zoom out
                interfaz.camara.zoom(0.05);
            break;
		case 'e': // activa/desactiva la visualizacion de los ejes
			interfaz.escena.set_ejes(interfaz.escena.get_ejes()?false:true);
        break;
        case 'w':
            
            interfaz.escena.avanzar(0.5);
            
            if(interfaz.escena.get_PersonajeX() + 2 <= interfaz.escena.getLimiteX() && interfaz.escena.get_PersonajeZ() + 2 <= interfaz.escena.getLimiteZ() && interfaz.escena.get_PersonajeX() > 2 && interfaz.escena.get_PersonajeZ() > 2){
                if(interfaz.camara.tipo==IGV_PERSONA){
                    interfaz.camara.set(IGV_PERSONA, igvPunto3D(interfaz.escena.get_PersonajeX(),1.0,interfaz.escena.get_PersonajeZ()),igvPunto3D(-sin(interfaz.escena.get_anguloPersonaje())*200,0,cos(interfaz.escena.get_anguloPersonaje())*200),igvPunto3D(0,1,0),
                                        -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
                    interfaz.camara.aplicar();
                }
            }else interfaz.escena.avanzar(-0.5);
            
            break;
        case 's': //retroceder
            
            interfaz.escena.avanzar(-0.5);
            
            if(interfaz.escena.get_PersonajeX() + 2 <= interfaz.escena.getLimiteX() && interfaz.escena.get_PersonajeZ() + 2 <= interfaz.escena.getLimiteZ() && interfaz.escena.get_PersonajeX() >= 2 && interfaz.escena.get_PersonajeZ() >= 2){
                
                if(interfaz.camara.tipo==IGV_PERSONA){
                    interfaz.camara.set(IGV_PERSONA, igvPunto3D(interfaz.escena.get_PersonajeX(),1.0,interfaz.escena.get_PersonajeZ()),igvPunto3D(-sin(interfaz.escena.get_anguloPersonaje())*200,0,cos(interfaz.escena.get_anguloPersonaje())*200),igvPunto3D(0,1,0),
                                        -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
                    interfaz.camara.aplicar();
                }
            }else interfaz.escena.avanzar(0.5);
            break;

        case 'a': //girar a la izquierda
            interfaz.escena.rotPersonaje(-0.4);
            if(interfaz.escena.get_PersonajeX() + 2 <= interfaz.escena.getLimiteX() && interfaz.escena.get_PersonajeZ() + 2 <= interfaz.escena.getLimiteZ() && interfaz.escena.get_PersonajeX() > 2 && interfaz.escena.get_PersonajeZ() > 2){
                if(interfaz.camara.tipo==IGV_PERSONA){
                    interfaz.camara.set(IGV_PERSONA, igvPunto3D(interfaz.escena.get_PersonajeX(),1.0,interfaz.escena.get_PersonajeZ()),igvPunto3D(-sin(interfaz.escena.get_anguloPersonaje())*200,0,cos(interfaz.escena.get_anguloPersonaje())*200),igvPunto3D(0,1,0),
                                        -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
                    interfaz.camara.aplicar();
                }
            }else interfaz.escena.rotPersonaje(0.4);
            break;

        case 'd': //girar a la derecha
            interfaz.escena.rotPersonaje(0.4);
            if(interfaz.escena.get_PersonajeX() + 2 <= interfaz.escena.getLimiteX() && interfaz.escena.get_PersonajeZ() + 2 <= interfaz.escena.getLimiteZ() && interfaz.escena.get_PersonajeX() > 2 && interfaz.escena.get_PersonajeZ() > 2){
                if(interfaz.camara.tipo==IGV_PERSONA){
                    interfaz.camara.set(IGV_PERSONA, igvPunto3D(interfaz.escena.get_PersonajeX(),1.0,interfaz.escena.get_PersonajeZ()),igvPunto3D(-sin(interfaz.escena.get_anguloPersonaje())*200,0,cos(interfaz.escena.get_anguloPersonaje())*200),igvPunto3D(0,1,0),
                                        -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
                    interfaz.camara.aplicar();}
            }else interfaz.escena.rotPersonaje(-0.4);
            break;
        case 'm':

            if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                interfaz.escena.getObjetos().push_back(new class Mesa(SUELO, 1,0.1,1,interfaz.escena.get_PersonajeX(), 0.1, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg"));
            }
            break;
        case 'l':
            if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                interfaz.escena.getObjetos().push_back(new class Silla(SUELO,0.5,0.1,0.5, interfaz.escena.get_PersonajeX(), 0.1, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg"));
            }
            break;
        case 'c':
            interfaz.escena.getObjetos().push_back(new class Cuadro(PARED1,1,0.5,
            2,1.5,conversionX,conversionY,conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/nieve.png"));
            break;
        case 'f':
            if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                interfaz.escena.getObjetos().push_back(new class Alfombra(SUELO,1,0,1, interfaz.escena.get_PersonajeX(), 0, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/nieve.png"));
            }
            break;
    case 27: // tecla de escape para SALIR
      exit(1);
    break;
  }
    interfaz.camara.aplicar();
	glutPostRedisplay(); // renueva el contenido de la ventana de vision
}

void igvInterfaz::set_glutReshapeFunc(int w, int h) {
  // dimensiona el viewport al nuevo ancho y alto de la ventana
  // guardamos valores nuevos de la ventana de visualizacion
  interfaz.set_ancho_ventana(w);
  interfaz.set_alto_ventana(h);

	// establece los par�metros de la c�mara y de la proyecci�n
	interfaz.camara.aplicar();
}

void igvInterfaz::set_glutDisplayFunc() {

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // borra la ventana y el z-buffer
    // se establece el viewport
    glViewport(0, 0, interfaz.get_ancho_ventana(), interfaz.get_alto_ventana());

    // Apartado A: antes de aplicar las transformaciones de c�mara y proyecci�n hay que comprobar el modo para s�lo visualizar o seleccionar:
    if (interfaz.modo == IGV_SELECCIONAR) {
        // Apartado A: Para que funcione habr� que dibujar la escena sin efectos, sin iluminaci�n, sin texturas ...
        glDisable(GL_LIGHTING); // desactiva la iluminacion de la escena
        glDisable(GL_DITHER);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_CULL_FACE);

        // Apartado A: Reestablece los colores como no seleccionado
        if(interfaz.objeto_seleccionado != -1)
            interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->limpiarSeleccion(); //Limpiamos los objetos seleccionados
        interfaz.objeto_seleccionado = -1;
        
        // Apartado A: aplica la c�mara
        interfaz.camara.aplicar();
        
        for(int i=0; i<interfaz.escena.getObjetos().size();i++){
            interfaz.escena.getObjetos()[i]->visualizar();
        }

        // Apartado A: Obtener el color del pixel seleccionado
        GLubyte *pixel = new GLubyte[3];
        glReadPixels(interfaz.cursorX, interfaz.alto_ventana-interfaz.cursorY, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
        std::cout << pixel[0] << "-" << pixel[1] << "-" << pixel[2] << std::endl;
        // Apartado A: Comprobar el color del objeto que hay en el cursor mirando en la tabla de colores y asigna otro color al objeto seleccionado
        for(int i=0; i<interfaz.escena.getObjetos().size();i++){
            std::cout << interfaz.escena.getObjetos()[i]->getColorByte()[0] << "-" << interfaz.escena.getObjetos()[i]->getColorByte()[1] << "-" << interfaz.escena.getObjetos()[i]->getColorByte()[2] << std::endl;
            if(interfaz.escena.getObjetos()[i]->getColorByte()[0] == pixel[0] &&
               interfaz.escena.getObjetos()[i]->getColorByte()[1] == pixel[1] &&
               interfaz.escena.getObjetos()[i]->getColorByte()[2] == pixel[2]){

                interfaz.escena.getObjetos()[i]->seleccionarObjeto(); //Pinta el objeto de color amarillo al seleccionar el objeto
                
                interfaz.objeto_seleccionado = i;

                break;
            }
        }
        
        // Apartado A: Cambiar a modo de visualizaci�n de la escena
        interfaz.modo = IGV_VISUALIZAR;

        // Apartado A: Habilitar de nuevo la iluminaci�n
        glEnable(GL_LIGHTING);
    }
    else {
        // aplica las transformaciones en funci�n de los par�metros de la c�mara
        interfaz.camara.aplicar();
        // visualiza la escena
        interfaz.escena.visualizar();

        // refresca la ventana
        glutSwapBuffers();
    }
    interfaz.create_menu(); //Recargamos el men�
}

void igvInterfaz::set_glutMouseFunc(GLint boton, GLint estado, GLint x, GLint y) {

    // Apartado A: comprobar que se ha pulsado el bot�n izquierdo
    if(boton == 0){
        // Apartado A: guardar que el boton se ha presionado o se ha soltado, si se ha pulsado hay que
        // pasar a modo IGV_SELECCIONAR
        if(estado == GLUT_DOWN){
            interfaz.modo = IGV_SELECCIONAR;
            interfaz.boton_retenido = true;

        }else interfaz.boton_retenido = false;
        
        // Apartado A: guardar el pixel pulsado
        interfaz.cursorX = x;
        interfaz.cursorY = y;
        
        // Apartado A: renovar el contenido de la ventana de vision
        glutPostRedisplay();
    }
}

void igvInterfaz::set_glutMotionFunc(GLint x, GLint y) {

    // Apartado B: si el bot�n est� retenido y hay alg�n objeto seleccionado,
    // comprobar el objeto seleccionado y la posici�n del rat�n y rotar
    // el objeto seleccionado utilziando el desplazamiento entre la posici�n
    //inicial y final del rat�n
    
    if(interfaz.boton_retenido && interfaz.objeto_seleccionado != -1){
        //Rotar
        interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->rotar(interfaz.cursorX - interfaz.cursorXfinal);
    }
    // Apartado B: guardar la nueva posici�n del rat�n
    interfaz.cursorX = x;
    interfaz.cursorY = y;
    // Apartado B: renovar el contenido de la ventana de vision
    glutPostRedisplay();
}

void igvInterfaz::inicializa_callbacks() {
    glutKeyboardFunc(set_glutKeyboardFunc);
    glutReshapeFunc(set_glutReshapeFunc);
    glutDisplayFunc(set_glutDisplayFunc);
    glutMouseFunc(set_glutMouseFunc);
    glutMotionFunc(set_glutMotionFunc);
    glutSpecialFunc(set_glutSpecialFunc);
}

void igvInterfaz::create_menu() {
    glutCreateMenu(menuHandle);
    
    if(interfaz.objeto_seleccionado != -1){ //Si hay un objeto seleccionado y es de tipo PARED
        if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() != SUELO){
            
            if(interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->getTipoObjeto() == CAMARA){
                glutAddMenuEntry("Camara de Vigilancia", 7);
            }else{
                //Objeto de tipo PARED que no es camaraVigilancia
                glutAddMenuEntry("Eliminar Objeto", 1);
                glutAddMenuEntry("Pared 1", 2);
                glutAddMenuEntry("Pared 2", 3);
                glutAddMenuEntry("Pared 3", 4);
                glutAddMenuEntry("Pared 4", 5);
                glutAddMenuEntry("Cambiar Textura", 6);
            }
        }else{
            glutAddMenuEntry("Eliminar Objeto", 1); //Tipo SUELO
            glutAddMenuEntry("Cambiar Textura", 12);
        }
    }else{
        glutAddMenuEntry("Crear Mesa", 8);
        glutAddMenuEntry("Crear Silla", 9);
        glutAddMenuEntry("Crear Cuadro", 10);
        glutAddMenuEntry("Crear Alfombra",11);
    }
    
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void igvInterfaz::menuHandle(int value){
    interfaz.menuSelection = value;

    int xColor = rand()%256, yColor = rand()%256, zColor = rand()%256;
    float conversionX = (float) xColor/255, conversionY = (float) yColor/255, conversionZ = (float) zColor/255;
    
    int texturaAleatoria = rand()%5;
    char* packTexturasCuadros[5] = {"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/estrellas.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/nieve.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/girasoles.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cazorla.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/andujar.jpg"};
    
    char* packTexturasSuelo[5] = {"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/liso.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/madera.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/sofa.jpg","/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/piel.jpg"};
    
    if(interfaz.objeto_seleccionado != -1){
        switch (interfaz.menuSelection) {
            case 1: //Eliminar objeto
                interfaz.escena.getObjetos().erase(interfaz.escena.getObjetos().begin() + interfaz.objeto_seleccionado);
                interfaz.objeto_seleccionado = -1;
                interfaz.menuSelection = 0;
                break;
            case 2: //Poner objeto en pared 1
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setTipoObjeto(PARED1);
                break;
            case 3: //Poner objeto en pared 2
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setTipoObjeto(PARED2);
                break;
            case 4: //Poner objeto en pared 3
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setTipoObjeto(PARED3);
                break;
            case 5: //Poner objeto en pared 4
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setTipoObjeto(PARED4);
                break;
            case 6:
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setRuta(packTexturasCuadros[texturaAleatoria]);
                break;
            case 7:
                interfaz.camara.set(IGV_PERSPECTIVA, igvPunto3D(19, 2.5, 19), igvPunto3D(0, 0, 0), igvPunto3D(0, 1.0, 0), -1 * 3, 1 * 3, -1 * 3, 1 * 3, 1, 200);
                break;
            case 12:
                interfaz.escena.getObjetos()[interfaz.objeto_seleccionado]->setRuta(packTexturasSuelo[texturaAleatoria]);
                break;
        }
    }else{
        switch (interfaz.menuSelection) {
            case 8:
                if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                    interfaz.escena.getObjetos().push_back(new class Mesa(SUELO, 1,0.1,1,interfaz.escena.get_PersonajeX(), 0.1, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/sofa.jpg"));
                }
                break;
            case 9:
                if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                    interfaz.escena.getObjetos().push_back(new class Silla(SUELO,0.5,0.1,0.5, interfaz.escena.get_PersonajeX(), 0.1, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg"));
                }
                break;
            case 10:
                interfaz.escena.getObjetos().push_back(new class Cuadro(PARED1,1,0.5,
                2,1.5,conversionX,conversionY,conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/nieve.png"));
                break;
            case 11:
                if(interfaz.escena.colision(-1,-1,interfaz.escena.get_PersonajeX(),interfaz.escena.get_PersonajeZ())){
                    interfaz.escena.getObjetos().push_back(new class Alfombra(SUELO,1,0,1, interfaz.escena.get_PersonajeX(), 0, interfaz.escena.get_PersonajeZ(),interfaz.escena.get_anguloPersonaje(),conversionX, conversionY, conversionZ,"/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/nieve.png"));
                }
                break;
        }
    }
    
    glutPostRedisplay(); // renew the content of the window
}
