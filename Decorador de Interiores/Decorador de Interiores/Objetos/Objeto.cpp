//
//  Objeto.cpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 23/12/20.
//

#include "Objeto.hpp"
#include "igvTextura.h"
#include <iostream>

Objeto::Objeto(tipoObjeto obj, float r, float g, float b): objeto(obj){  //Objetos de tipo camaraVigilancia
    ruta = "/Users/pedrosaez/Documents/proyecto-decorador-de-interiores-igv/Decorador\ de\ Interiores/Decorador\ de\ Interiores/Texturas/cuero.jpg";
    
    color = new float[3];
    color[0] = r;
    color[1] = g;
    color[2] = b;
    
    seleccionado = false;
    colorAmarillo = new float[3];
    colorAmarillo[0] = 1; colorAmarillo[1] = 1; colorAmarillo[2] = 0;
    anguloRotar = 0;
}

Objeto::Objeto(tipoObjeto _objeto, float dimX, float dimY, float dimZ, float _posX, float _posY, float _posZ, float angulo, float r, float g, float b){
    
    objeto = _objeto;
    
    x = dimX; y = dimY; z = dimZ;
    posX = _posX; posY = _posY; posZ = _posZ;
    anguloObjeto = angulo;
    
    color = new float[3];
    color[0] = r;
    color[1] = g;
    color[2] = b;
    seleccionado = false;
    colorAmarillo = new float[3];
    colorAmarillo[0] = 1; colorAmarillo[1] = 1; colorAmarillo[2] = 0;
    anguloRotar = 0;
    
    ruta = "";
    
}

Objeto::Objeto(tipoObjeto _objeto, float dimX, float dimY, float dimZ, float _posX, float _posY, float _posZ, float angulo, float r, float g, float b, char *_ruta){
    
    objeto = _objeto;
    
    x = dimX; y = dimY; z = dimZ;
    posX = _posX; posY = _posY; posZ = _posZ;
    anguloObjeto = angulo;
    
    color = new float[3];
    color[0] = r;
    color[1] = g;
    color[2] = b;
    seleccionado = false;
    colorAmarillo = new float[3];
    colorAmarillo[0] = 1; colorAmarillo[1] = 1; colorAmarillo[2] = 0;
    anguloRotar = 0;
    
    ruta = _ruta;
    
}

Objeto::Objeto(tipoObjeto _objeto, float dimX, float dimY, float _posX, float _posY, float r, float g, float b, char *_ruta){
    
    objeto = _objeto;
    
    x = dimX; y = dimY,
    posX = _posX; posY = _posY;
    anguloObjeto = 0.0;
    
    color = new float[3];
    color[0] = r;
    color[1] = g;
    color[2] = b;
    seleccionado = false;
    colorAmarillo = new float[3];
    colorAmarillo[0] = 1; colorAmarillo[1] = 1; colorAmarillo[2] = 0;
    anguloRotar = 0;
    ruta = _ruta;
}

Objeto::~Objeto() {
    delete color;
};

//MÈtodo para visualizar una caja

float* Objeto::getColor() {
    return color;
}

GLubyte* Objeto::getColorByte() {
    GLubyte colorubyte[3];
    colorubyte[0] = (GLubyte)(color[0] * 255);
    colorubyte[1] = (GLubyte)(color[1] * 255);
    colorubyte[2] = (GLubyte)(color[2] * 255);
    
    return colorubyte;
}

void Objeto::seleccionarObjeto(){
    seleccionado = true;
    visualizar();
}

void Objeto::limpiarSeleccion(){
    seleccionado = false;
    visualizar();
}

void Objeto::rotar(float angulo){
    if(objeto == SUELO){
        anguloRotar = angulo;
        visualizar();
    }
}

void Objeto::mover(float x, float z){
    posX += x; posZ += z;
}

void Objeto::moverPared(float x, float y, float z){
    posY += y; posX += x;
}

float Objeto::getAngulo(){
    return anguloObjeto;
}

float Objeto::getPosX(){
    return posX;
}

float Objeto::getPosY(){
    return posY;
}

float Objeto::getPosZ(){
    return posZ;
}

tipoObjeto Objeto::getTipoObjeto(){
    return objeto;
}

void Objeto::setTipoObjeto(tipoObjeto obj){
    objeto = obj;
}

float Objeto::tamX(){ return x;}
float Objeto::tamY(){ return y;}
float Objeto::tamZ(){ return z;}

char* Objeto::getRuta(){ return ruta; }

void Objeto::setRuta(char *_ruta){ ruta = _ruta; }

bool Objeto::operator==(const Objeto &obj){
    if(this->posX == obj.posX && this->posZ == obj.posZ) return true;
    
    return false;
}
