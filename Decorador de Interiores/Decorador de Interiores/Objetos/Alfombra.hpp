//
//  Alfombra.hpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 28/12/20.
//

#ifndef Alfombra_hpp
#define Alfombra_hpp

#include <stdio.h>
#include "Objeto.hpp"

class Alfombra : public Objeto{
public:
    
    Alfombra(tipoObjeto _objeto, float _x, float _y, float _z, float PosX, float PosY, float PosZ, float angulo, float r, float g, float b, char *_ruta);
    ~Alfombra(){}
    
    void visualizar() override; //Dibuja una mesa con los atributos especificados en el constructor (si los hay)
};

#endif /* Alfombra_hpp */
