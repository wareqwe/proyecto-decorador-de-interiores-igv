//
//  Objeto.hpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 23/12/20.
//

#ifndef Objeto_hpp
#define Objeto_hpp

#include <stdio.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif

typedef enum {
    SUELO,  //Objetos que están en el suelo
    PARED1, //Rango (x,0)
    PARED2, //Rango (xMax, z)
    PARED3, //Rango (x, Zmax)
    PARED4,  //Rango (0, z)
    CAMARA
} tipoObjeto;

class Objeto {
    
protected:
    tipoObjeto objeto;
    float* color;
    float *colorAmarillo;
    bool seleccionado = false;
    float anguloRotar;
    float x,y,z; //Dimensiones del objeto
    float posX, posY, posZ; //Posición donde ubicaremos el objeto en la escena;
    float anguloObjeto; //Ángulo donde está orientado el objeto
    char *ruta;
    
public:
    Objeto(tipoObjeto obj, float r, float g, float b); //Objetos de tipo camaraVigilancia
    Objeto(tipoObjeto _objeto, float dimX, float dimY, float dimZ, float posX, float posY, float posZ, float angulo, float r, float g, float b);
    Objeto(tipoObjeto _objeto, float dimX, float dimY, float dimZ, float posX, float posY, float posZ, float angulo, float r, float g, float b, char *_ruta);
    Objeto(tipoObjeto _objeto, float dimX, float dimY, float posX, float posY, float r, float g, float b, char *_ruta);
    ~Objeto();

    //MÈtodo para visualizar una caja
    virtual void visualizar() = 0;

    //Devuelve el color
    float* getColor();
    GLubyte* getColorByte(); //Para comparar con el pixel obtenido en la selecciÛn
    float getAngulo();
    float getPosX();
    float getPosY();
    float getPosZ();
    float tamX();
    float tamY();
    float tamZ();
    tipoObjeto getTipoObjeto();
    char* getRuta();
    
    void setTipoObjeto(tipoObjeto obj);
    void setRuta(char *ruta);
    
    void seleccionarObjeto();
    void limpiarSeleccion();
    void rotar(float angulo);
    void mover(float x, float z); //Objetos que se mueven en el suelo de la habitación
    void moverPared(float x, float y, float z); //Objetos que solo pueden moverse en pared
    
    bool operator==(const Objeto &obj);
};

#endif
