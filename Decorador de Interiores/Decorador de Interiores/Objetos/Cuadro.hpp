//
//  Cuadro.hpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 27/12/20.
//

#ifndef Cuadro_hpp
#define Cuadro_hpp

#include <stdio.h>
#include "Objeto.hpp"

class Cuadro : public Objeto{
public:
    
    Cuadro(tipoObjeto _objeto, float _x, float _y, float PosX, float PosY, float r, float g, float b, char *ruta);
    ~Cuadro(){}
    
    void visualizar() override; //Dibuja una mesa con los atributos especificados en el constructor (si los hay)
    
};

#endif /* Cuadro_hpp */
