//
//  CamaraVigilancia.hpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 28/12/20.
//

#ifndef CamaraVigilancia_hpp
#define CamaraVigilancia_hpp

#include <stdio.h>
#include "Objeto.hpp"

class CamaraVigilancia : public Objeto{
public:
    CamaraVigilancia(tipoObjeto _objeto, float r, float g, float b);
    ~CamaraVigilancia(){}
    
    void visualizar() override; //Dibuja una mesa con los atributos especificados en el constructor (si los hay)

};

#endif /* CamaraVigilancia_hpp */
