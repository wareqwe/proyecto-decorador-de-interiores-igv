//
//  Mesa.cpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 24/12/20.
//

#include "Mesa.hpp"
#include "igvMaterial.h"
#include "igvColor.h"
#include <math.h>

Mesa::Mesa(tipoObjeto _objeto, float _x, float _y, float _z, float PosX, float PosY, float PosZ, float angulo, float r, float g, float b, char *_ruta)
:Objeto(_objeto, _x, _y, _z, PosX, PosY, PosZ, angulo, r, g, b, _ruta){}

void Mesa::visualizar(){

    if(!seleccionado){
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
        glColor3fv(color);//Importante, es necesario para quelo reconozca el buffer de color a la hora de la selecciÛn.
    }
    else{//Si está seleccionado, la caja se pinta en amarillo
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorAmarillo);
        glColor3fv(colorAmarillo);
    }

    glPushMatrix();
    glTranslatef(-sin(anguloObjeto)*2, 0.0, cos(anguloObjeto)*2);
    glTranslatef(posX, posY, posZ);
    glRotatef(anguloRotar, 0, 1, 0);
        //Tablero de la mesa
        glPushMatrix();
    
            glScalef(x, y, z);
            
            glNormal3f(0,1,0);
            glBegin(GL_QUADS);

            glTexCoord2f(0,1); glVertex3f(-0.5,0,-0.5);
            
            glTexCoord2f(0,0); glVertex3f(-0.5,0,0.5);
            
            glTexCoord2f(1,0); glVertex3f(0.5,0,0.5);
            
            glTexCoord2f(1,1); glVertex3f(0.5,0,-0.5);
    
            glEnd();

        glPopMatrix();
        
        //Dibujo de las patas de la mesa
        glPushMatrix();
            GLUquadricObj *objetivo=gluNewQuadric();
            glPushMatrix();
                glTranslatef((-x/2)+0.05, 0, (-z/2)+0.05);
                glRotatef(90, 1, 0, 0);
                gluCylinder(objetivo,0.05,0.05,0.1,20,20);
            glPopMatrix();

            glPushMatrix();
                glTranslatef((x/2)-0.05, 0, (z/2)-0.05);
                glRotatef(90, 1, 0, 0);
                gluCylinder(objetivo,0.05,0.05,0.1,20,20);
            glPopMatrix();

            glPushMatrix();
                glTranslatef((x/2)-0.05, 0, (-z/2)+0.05);
                glRotatef(90, 1, 0, 0);
                gluCylinder(objetivo,0.05,0.05,0.1,20,20);
            glPopMatrix();

            glPushMatrix();
                glTranslatef((-x/2)+0.05, 0, (z/2)-0.05);
                glRotatef(90, 1, 0, 0);
                gluCylinder(objetivo,0.05,0.05,0.1,20,20);
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
    
}
