//
//  Alfombra.cpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 28/12/20.
//

#include "Alfombra.hpp"

Alfombra::Alfombra(tipoObjeto _objeto, float _x, float _y, float _z, float PosX, float PosY, float PosZ, float angulo, float r, float g, float b, char* _ruta)
:Objeto(_objeto, _x, _y, _z, PosX, PosY, PosZ, angulo, r, g, b,_ruta){}

void Alfombra::visualizar(){
    glPushMatrix();
    
    glTranslatef(Objeto::posX, 0.1, Objeto::posZ);
    
    if(!seleccionado){
        glMaterialfv(GL_FRONT, GL_EMISSION, color);
        glColor3fv(color);//Importante, es necesario para quelo reconozca el buffer de color a la hora de la selecciÛn.
    }
    else{//Si está seleccionado, el objeto se pinta en amarillo
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorAmarillo);
        glColor3fv(colorAmarillo);
    }
    
    glNormal3f(0,1,0);
    glBegin(GL_QUADS);
    glNormal3f(0,1,0);
    glBegin(GL_QUADS);

    glTexCoord2f(0,1); glVertex3f(-0.5,0,-0.5);
    
    glTexCoord2f(0,0); glVertex3f(-0.5,0,0.5);
    
    glTexCoord2f(1,0); glVertex3f(0.5,0,0.5);
    
    glTexCoord2f(1,1); glVertex3f(0.5,0,-0.5);

    glEnd();
    glEnd();
    glPopMatrix();
}
