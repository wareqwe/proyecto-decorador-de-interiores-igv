//
//  CamaraVigilancia.cpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 28/12/20.
//

#include "CamaraVigilancia.hpp"

CamaraVigilancia::CamaraVigilancia(tipoObjeto obj, float r, float g, float b): Objeto(obj,r,g,b){}

void CamaraVigilancia::visualizar(){
    
    if(!seleccionado){
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, color);
        glColor3fv(color);//Importante, es necesario para quelo reconozca el buffer de color a la hora de la selecciÛn.
    }
    else{//Si está seleccionado, el objeto se pinta en amarillo
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorAmarillo);
        glColor3fv(colorAmarillo);
    }
    
    glPushMatrix();
        glTranslatef(19, 2.5, 19);
        glRotatef(225, 0, 1, 0);
        glRotatef(20, 1, 0, 0);
        glScalef(0.2, 0.2, 0.2);

        glPushMatrix();
            glScalef(1, 1, 2);
            glutSolidCube(1);
        glPopMatrix();
        //Pintamos el objetivo de la cámara
        glPushMatrix();
            GLUquadricObj *objetivo=gluNewQuadric();

            glPushMatrix();
                glTranslatef(0, 0, 1);
                gluCylinder(objetivo,0.3,0.6,1.3,20,20);
            glPopMatrix();

            gluDeleteQuadric(objetivo);
        glPopMatrix();
    
    glPopMatrix();
}
