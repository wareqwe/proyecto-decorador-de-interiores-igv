//
//  Cuadro.cpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 27/12/20.
//

#include "Cuadro.hpp"
#include "igvTextura.h"
#include "igvMaterial.h"
#include "igvColor.h"

Cuadro::Cuadro(tipoObjeto _objeto, float _x, float _y, float PosX, float PosY, float r, float g, float b, char *_ruta)
:Objeto(_objeto, _x, _y, PosX, PosY, r, g, b, _ruta){}

void Cuadro::visualizar(){
    int divX = 50, divy = 50; //Número de divisiones
    
    float inicioX = -Objeto::x / 2; //Posición de comienzo para crear los vértices en eje X
    float inicioY = -Objeto::y / 2; //Posición de comienzo para crear los vértices en eje Y
    //El eje Z será la separación del cuadro con la pared, por defecto siempre será el mismo
    
    //Distancia entre vértices (50 divisiones)
    float incrementoX = Objeto::x / divX;
    float incrementoY  = Objeto::y / divy;
    
    //Distancia entre vértices de la textura
    float incrementoTexturaX = (float) 1 / divX;
    float incrementoTexturaY = (float) 1 / divy;
    
    //Posición de los vértices de la textura
    float posTexturaX = 0;
    float posTexturaY = 1;
    
    glPushMatrix();
    
    if(!seleccionado){
        glMaterialfv(GL_FRONT, GL_EMISSION, color);
        glColor3fv(color);//Importante, es necesario para quelo reconozca el buffer de color a la hora de la selecciÛn.
    }
    else{//Si está seleccionado, el objeto se pinta en amarillo
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, colorAmarillo);
        glColor3fv(colorAmarillo);
    }
    
    glPushMatrix();
    
    switch (Objeto::objeto) {
        case PARED1:
            glTranslatef(Objeto::posX, Objeto::posY, 0.1);
            break;
        case PARED2:
            glTranslatef(19.9, Objeto::posY, Objeto::posX);
            break;
        case PARED3:
            glTranslatef(Objeto::posX, Objeto::posY, 19.9);
            break;
        case PARED4:
            glTranslatef(0.1, Objeto::posY, Objeto::posX);
            break;
    }
    
    if(Objeto::objeto == PARED1 || Objeto::objeto == PARED3){
        glRotatef(90, 1, 0, 0);
    }
    else{
        glRotatef(90, 0, 0, 1);
        glRotatef(90, 0, 1, 0); //Hay que rotar primero en el eje Y para orientarlo sino el tamaño X e Y serían inversos
    }
    
    glNormal3f(0,1,0);
    glBegin(GL_QUADS);
    for(float i = inicioX; i<Objeto::x; i += incrementoX, posTexturaX += incrementoTexturaX){
        posTexturaY = 1;
        for(float j = inicioY; j<Objeto::y; j+= incrementoY, posTexturaY -= incrementoTexturaY){
            glTexCoord2f(posTexturaX,posTexturaY);
            glVertex3f(i, 0, j);
            
            glTexCoord2f(posTexturaX,posTexturaY - incrementoTexturaY);
            glVertex3f(i, 0, j+incrementoY);
            
            glTexCoord2f(posTexturaX + incrementoTexturaX,posTexturaY - incrementoTexturaY);
            glVertex3f(i+incrementoX, 0, j+incrementoY);
            
            glTexCoord2f(posTexturaX + incrementoTexturaX,posTexturaY);
            glVertex3f(i+incrementoX, 0, j);
        }
    }
    glEnd();
    glPopMatrix();
    
    glPopMatrix();
}
