//
//  Silla.hpp
//  Decorador de Interiores
//
//  Created by Pedro Saez on 24/12/20.
//

#ifndef Silla_hpp
#define Silla_hpp

#include <stdio.h>
#include "Objeto.hpp"

class Silla : public Objeto{
public:
    Silla(tipoObjeto _objeto, float _x, float _y, float _z, float PosX, float PosY, float PosZ, float angulo, float r, float g, float b, char *_ruta);
    ~Silla();
    
    void visualizar() override;
    
};
#endif /* Silla_hpp */
