
#include "Objeto.hpp"

class Mesa : public Objeto{

public:
    
    Mesa(tipoObjeto _objeto, float _x, float _y, float _z, float PosX, float PosY, float PosZ, float angulo, float r, float g, float b, char *_ruta);
    ~Mesa(){}
    
    void visualizar() override; //Dibuja una mesa con los atributos especificados en el constructor (si los hay)
};
